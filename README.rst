.. -*- restructuredtext -*-

===============================
Master Computer Science Seminar
===============================
3 reports of `INFO-F530 Computer Science Seminar`_ (ULB): (19/20)

.. _`INFO-F530 Computer Science Seminar`: https://web.archive.org/web/20180601005154/https://www.ulb.ac.be/di/map/tlenaert/Home_Tom_Lenaerts/INFO-F-530.html

1. `Cache-Oblivious Algorithms & Data Structures`_

2. `Blockchain, Opportunities and challenges`_

6. `Utilisation de plateformes ludiques de programmation pour motiver les apprenants`_

.. _`Cache-Oblivious Algorithms & Data Structures`: https://bitbucket.org/OPiMedia/master-computer-science-seminar/src/master/1_Cache/INFO-F530-seminar-1--Cache-oblivious--Olivier-Pirson.pdf
.. _`Blockchain, Opportunities and challenges`: https://bitbucket.org/OPiMedia/master-computer-science-seminar/src/master/2_Blockchain/INFO-F530-seminar-2--Blockchain--Olivier-Pirson.pdf
.. _`Utilisation de plateformes ludiques de programmation pour motiver les apprenants`: https://bitbucket.org/OPiMedia/master-computer-science-seminar/src/master/6_ludique/INFO-F530-seminar-6--plateformes-ludiques-de-programmation--Olivier-Pirson.pdf



All documents and LaTeX sources are available on this Bitbucket repository:
https://bitbucket.org/OPiMedia/master-computer-science-seminar

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|Master Computer Science Seminar|

(picture from `Cache-Oblivious Algorithms and Data Structures`_)

.. _`Cache-Oblivious Algorithms and Data Structures`: https://web.archive.org/web/20180601125033/https://www.ulb.ac.be/di/map/tlenaert/Home_Tom_Lenaerts/INFO-F-530_files/DLS%20John%20Iacono.pdf

.. |Master Computer Science Seminar| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/15/2390267381-1-master-computer-science-seminar-logo_avatar.png
