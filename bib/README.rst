.. -*- restructuredtext -*-

============
Bibliography
============
In BibLaTeX and Zotero format.
(My Zotero profile: opimedia_)

.. _opimedia: https://www.zotero.org/opimedia
